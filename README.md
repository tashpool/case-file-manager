# Case File Manager Client

## Overview
A Case File Management system, CFM, is used to enter and create reports on incidents.

A detective walks onto a crime scene and begins taking notes. Messy and disorganized notes that they have to put together back at the office taking hours, or even days.
Trying to put together that thriller novel and figure out whodunit? Map it out with individual events, that contain actors, evidence, and links to them!

This project is a mostly static Vue front end to a fully functional backend API written in Node. It was created to help teach myself more about web app creation and deployment. Definitely a work in progress.

## Project setup Node
```
npm install
```
Then create Vue front end
```
npm run build
```
Finally, run with
```
npm run start
```

### Compiles and hot-reloads for development
```
npm run dev
```



### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### API Documentation
[https://documenter.getpostman.com/view/8054197/T1Dv9FUJ](https://documenter.getpostman.com/view/8054197/T1Dv9FUJ)

### Author and Repo
[https://gitlab.com/tashpool/case-file-manager](https://gitlab.com/tashpool/case-file-manager)