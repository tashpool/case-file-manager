class ApiFiltering {
  constructor(query, queryString) {
    this.query = query
    this.queryString = queryString
  }

  /*
  Remove all fields listed in excludedFields,
  Rebuild the query with proper Mongo syntax for filters, gte -> $gte
   */
  filter() {
    const queryObj = {...this.queryString}
    const excludedFields = ['page', 'sort', 'limit', 'fields']

    excludedFields.forEach(el => delete queryObj[el])

    let queryAsString = JSON.stringify(queryObj)
    queryAsString = queryAsString.replace(/\b(gte|gt|lte|lt)\b/g, match => `$${match}`)

    this.query = this.query.find(JSON.parse(queryAsString))

    return this
  }

  // TODO createdAt exist in DB?
  sort() {
    if (this.queryString.sort) {
      const sortBy = this.queryString.sort.split(',').join(' ')
      this.query = this.query.sort(sortBy)
    } else {
      this.query = this.query.sort('-createdAt')
    }

    return this
  }

  // TODO do we need a default select on version field?
  limitFields() {
    if (this.queryString.fields) {
      const fields = this.queryString.fields.split(',').join(' ')
      this.query = this.query.select(fields)
    } else {
      this.query = this.query.select('-__v')
    }

    return this
  }

  paginate() {
    const page = Number(this.queryString.page) || 1
    const limit = Number(this.queryString.limit) || 3
    const skip = (page - 1) * limit

    this.query = this.query.skip(skip).limit(limit)

    return this
  }
}

module.exports = ApiFiltering