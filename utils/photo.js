const multer = require('multer')
const sharp = require('sharp')
const AppError = require('./appError')
const fs = require('fs').promises

// keep image in memory for resize
const multerStorage = multer.memoryStorage()

const multerFilter = (req, file, cb) => {
  // is this file an image? cb( true | false with error)
  if(file.mimetype.startsWith('image')) {
    cb(null, true)
  } else {
    cb(new AppError('Not an image, please upload only images.', 400), false)
  }
}

const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter
})

exports.uploadPhoto = upload.single('photo')

// TODO add checks for: actors | users
exports.resizePhoto = function(imageProfile) {
  return (req, res, next) => {
    // file is on req obj if present
    if (!req.file) return next()

    let userID = 'unknownUser'
    req.params.id ? userID = req.params.id : userID = req.user.id

    // TODO req.user.id is incorrect - need actor NOT logged in user
    req.file.filename = `${imageProfile}-${userID}-${Date.now()}.jpeg`

    sharp(req.file.buffer)
      .resize(500, 500)
      .toFormat('jpeg')
      .jpeg({quality: 90})
      .toFile(`public/img/${imageProfile}/${req.file.filename}`)

    next()
  }
}

exports.deletePhoto = async function(photoPath) {
  try {
    await fs.unlink(photoPath)
  } catch(err) {
    new AppError('Could not delete avatar.', 500)
    // console.log(`Error 💥 FNF: ${photoPath}`)
  }
}