const express = require('express')
const cors = require('cors')
const morgan = require('morgan')
const AppError = require('./../utils/appError')
const globalErrorHandler = require('./../controllers/errorController')
const rateLimit = require('express-rate-limit')
const helmet = require('helmet')
const mongoSanitize = require('express-mongo-sanitize')
const xss = require('xss-clean')
const hpp = require('hpp')
const path = require('path')
const serveStatic = require('serve-static')

// --- Routes
const userRouter = require('../routes/userRouter')
const caseRouter = require('../routes/caseRouter')
const actorRouter = require('../routes/actorRouter')
const evidenceRouter = require('../routes/evidenceRouter')
const eventRouter = require('../routes/eventRouter')

// --- Express Config and Execution
const app = express()

// Needs to be added ASAP for headers to take precedence
app.use(helmet())

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'))
}

const limiter = rateLimit({
  max: 1000,
  windowMs: 60 * 60 * 1000,
  message: 'Too many requests from this IP, please try again in an hour.'
})
// only affect /api/* routes
app.use('/api', limiter)

app.use(cors())
app.use(express.json({
  limit: '100kb'
}))
app.use(express.urlencoded({ extended: false }))

// Serve images
app.use(express.static(path.join(__dirname, '../public')))

// ---- Vue Front End Integration
app.use(serveStatic(path.join(__dirname, '../client/dist/')))

// this * route is to serve project on different page routes except root `/`
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '../client/dist/index.html'))
})

// --- Sanitization
// Data san against NoSQL query injections
// mongoSanitize() returns a function that becomes middleware
app.use(mongoSanitize())
// Data san against XSS
// clean HTML or JS code; all symbols are converted
app.use(xss())
// HTTP Param Pollution
// MUST be after url encoding
app.use(hpp({
  whitelist: [
    'duration',
    'ratingsQuantity',
    'ratingsAverage',
    'maxGroupSize',
    'difficulty',
    'price'
  ]
}))

// --- Routers ---
app.use('/api/users', userRouter)
app.use('/api/cases', caseRouter)
app.use('/api/actors', actorRouter)
app.use('/api/evidences', evidenceRouter)
app.use('/api/events', eventRouter)

// Catch all error handler at END of all routes
app.all('*', (req, res, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on server.`, 404))
})

// If you are throwing an error with next(), this is where it goes (4 params)
app.use( globalErrorHandler )

module.exports = app