process.on('uncaughtException', err => {
  console.log('***UNCAUGHT EXCEPTION*** Shutting down.')
  console.log(err.name, err.message)
  process.exit(1)
})

if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}
const app = require('./app')

// --- Mongo Connection Setup
const mongoose = require('mongoose')
const mongoConnectString = process.env.mongoDB.replace(
  '<PASSWORD>',
  process.env.mongoPassword
)

mongoose.connect(mongoConnectString,
  {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
  .catch(error => console.error(error))

let db = mongoose.connection
db.once('open', () => {
  console.log('MongoDB DB connection established successfully.')
})
db.on('error', console.error.bind(console, 'MongoDB connection error: '))
// --- End Mongo Connection Setup

// --- Server
const port = Number(process.env.PORT) || 8081
const server = app.listen(port, () => {
  console.log(`CFM starting on port ${port}.`);
})

process.on('unhandledRejection', err => {
  console.error('***UNHANDLED REJECTION*** Shutting down.')
  console.log(err.name, err.message)
  server.close(() => {
    process.exit(1)
  })
})