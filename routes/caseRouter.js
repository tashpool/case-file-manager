const express = require('express')
const authController = require('./../controllers/authController')
const caseController = require('./../controllers/CFMCaseController')

const router = express.Router()

router.route('/')
  .get(caseController.cfmcase_list)
  .post(authController.protect,
    authController.restrictTo('admin', 'user'),
    caseController.cfmcase_create)
router.route('/:id')
  .get(caseController.cfmcase_detail)
  .patch(authController.protect,
    caseController.cfmcase_update)
  .delete(authController.protect,
    caseController.cfmcase_delete)
router.route('/:id/actors')
  .get(caseController.cfmcase_get_actors)

module.exports = router