const express = require('express')
const authController = require('./../controllers/authController')
const actorController = require('./../controllers/CFMActorController')
const photoHandler = require('./../utils/photo')

const router = express.Router()

// USE for all routes below
// router.use(authController.protect)

router.route('/')
  .get(actorController.cfmactor_list)
  .post(authController.protect,
    actorController.cfmactor_create)

router
  .route('/:id')
  .get(actorController.cfmactor_detail)
  .patch(
    authController.protect,
    photoHandler.uploadPhoto,
    photoHandler.resizePhoto('actors'),
    actorController.cfmactor_update)
  .delete(authController.protect,
    actorController.cfmactor_delete)

module.exports = router