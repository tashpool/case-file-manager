const express = require('express')
const authController = require('./../controllers/authController')
const eventController = require('./../controllers/CFMEventController')

const router = express.Router()

router.route('/')
  .get(authController.protect,
    eventController.cfmevent_list)
  .post(authController.protect,
    eventController.cfmevent_create)

// Order matters, after id route this would throw an error
router
  .route('/range')
  .post(eventController.cfmevent_getsome)

router
  .route('/:id')
  .get(eventController.cfmevent_detail)
  .patch(authController.protect,
    eventController.cfmevent_update)
  .delete(authController.protect,
    eventController.cfmevent_delete)

module.exports = router