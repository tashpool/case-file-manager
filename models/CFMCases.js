const mongoose = require('mongoose')
const Schema = mongoose.Schema

const CaseSchema = new Schema({
  owner: { type: Schema.Types.ObjectID, ref: 'CFMUser' },
  event: [{ type: Schema.Types.ObjectID, ref: 'CFMEvent'}],
  title: { type: String, required: true, max: 100 },
  description: { type: String, max: 500 },
  openDate: { type: Date, default: Date.now, required: true },
  closeDate: { type: Date }
})

module.exports = mongoose.model('CFMCase', CaseSchema)