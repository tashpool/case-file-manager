const mongoose = require('mongoose')
const Schema = mongoose.Schema

let ActorSchema = new Schema({
  foreName: { type: String, max: 100 },
  surName: { type: String, max: 100 },
  gender: { type: String,
    enum: ['male', 'female', 'non-binary', 'unknown'],
    default: 'unknown'
  },
  description: { type: String, max: 255 },
  title: { type: String, required: false, max: 50 },
  birthDate: { type: Date },
  deathDate: { type: Date },
  photo: {
    type: String,
    default: 'default_actor.jpeg'
  }
})

ActorSchema.virtual('name').get(function() {
  let fullName = 'Unknown'
  if (this.foreName && this.surName) {
    fullName = this.surName + ', ' + this.foreName
  }
  return fullName
})

module.exports = mongoose.model('CFMActor', ActorSchema)