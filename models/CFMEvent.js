let mongoose = require('mongoose')
let Schema = mongoose.Schema

let EventSchema = new Schema({
  title: { type: String, required: true, max: 500 },
  activity: { type: String, max: 50 },
  description: { type: String, required: true, max: 5000 },
  actor: [{ type: Schema.Types.ObjectId, ref: 'CFMActor' }],
  startTime: { type: Date, default: Date.now(), required: true },
  endTime: { type: Date },
  location: {
    type: {
      type: String,
      default: 'Point',
      enum: ['Point']
    },
    coordinates: [Number],
    description: { type: String, default: "" }
  },
  evidence: [{ type: Schema.Types.ObjectId, ref: 'CFMEvidence' }],
  linkedCase: { type: Schema.Types.ObjectId, ref: 'CFMCases'}
})

module.exports = mongoose.model('CFMEvent', EventSchema)