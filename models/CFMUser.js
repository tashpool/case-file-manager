const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs')
const crypto = require('crypto')

let UserSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Name is required.']
  },
  email: {
    type: String,
    required: [true, 'Email is required.'],
    unique: true,
    lowercase: true
  },
  photo: {
    type: String,
    default: 'default_avatar.jpeg'
  },
  role: {
    type: String,
    enum: ['user', 'admin'],
    default: 'user'
  },
  password: {
    type: String,
    required: [true, 'Password is required.'],
    minLength: 8,
    select: false
  },
  passwordConfirm: {
    type: String,
    required: [true, 'Please confirm your password.'],
    validate: {
      validator: function(passConfirm) {
        return passConfirm === this.password
      },
      message: 'Passwords are not the same.'
    }
  },
  passwordChangedAt: Date,
  passwordResetToken: String,
  passwordResetExpires: Date,
  active: {
    type: Boolean,
    default: true,
    select: false
  }
})

UserSchema.pre('save', async function(next) {
  if (!this.isModified('password')) return next()

  this.password = await bcrypt.hash(this.password, 12)
  this.passwordConfirm = undefined

  next()
})

/*
When modifying the password of a user, set the changed at property
behind by a second. Connection issues with tokens being issued
before a password change - which invalidates them immediately.
 */
UserSchema.pre('save', function (next) {
  if (!this.isModified('password') || this.isNew) return next()

  // Password one second in the past (HACK!) to ensure token created AFTER pass changed.
  this.passwordChangedAt = Date.now() - 1000
  next()
})

/*
When using **any** find query, do not return an inactive account
 */
UserSchema.pre(/^find/, function (next) {
  this.find({ active: { $ne: false } })
  next()
})

UserSchema.methods.correctPassword = async function(candidatePassword, password) {
  // this.password - NOT available, hidden field
  return await bcrypt.compare(candidatePassword, password)
}

UserSchema.methods.changedPasswordAfter = function(JWTTimestamp) {
  if (this.passwordChangedAt) {
    const changedTimestamp = parseInt(this.passwordChangedAt.getTime() / 1000, 10)

    return JWTTimestamp < changedTimestamp
  }

  // Default to false means no password was changed
  return false
}

UserSchema.methods.createPasswordResetToken = function() {
  const resetToken = crypto.randomBytes(32).toString('hex')
  this.passwordResetToken = crypto.createHash('sha256')
    .update(resetToken)
    .digest('hex')

  this.passwordResetExpires = Date.now() + 10 * 60 * 1000

  return resetToken
}

module.exports = mongoose.model('CFMUser', UserSchema)