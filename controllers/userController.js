const User = require('./../models/CFMUser')
const catchAsync = require('./../utils/catchAsync')
const appError = require('./../utils/appError')
const routeFactory = require('./../controllers/handlerFactory')
const multer = require('multer')
const AppError = require('../utils/appError')
const sharp = require('sharp')
const { deletePhoto } = require('./../utils/photo')

/*
There are some fields that should not be included when querying the DB
 */
const filterObj = (obj, ...allowedFields) => {
  const tempObject = {}
  Object.keys(obj).forEach(el => {
    if (allowedFields.includes(el)) tempObject[el] = obj[el]
  })

  return tempObject
}

exports.getMe = (req, res, next) => {
  req.params.id = req.user.id
  next()
}

// -- Image Upload Begin --
// keep image in memory for resize
const multerStorage = multer.memoryStorage()

const multerFilter = (req, file, cb) => {
  // is this file an image? cb( true | false with error)
  if(file.mimetype.startsWith('image')) {
    cb(null, true)
  } else {
    cb(new AppError('Not an image, please upload only images.', 400), false)
  }
}

const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter
})

exports.uploadUserPhoto = upload.single('photo')

exports.resizeUserPhoto = (req, res, next) => {
  // file is on req obj if present
  if (!req.file) return next()

  req.file.filename = `user-${req.user.id}-${Date.now()}.jpeg`

  sharp(req.file.buffer)
    .resize(500, 500)
    .toFormat('jpeg')
    .jpeg({ quality: 90 })
    .toFile(`public/img/users/${req.file.filename}`)

  next()
}
//-- End Image Upload --

/*
This route is **ONLY** for updating user info, not for passwords
 */
exports.updateMe = catchAsync( async(req, res, next) => {
  if (req.body.password || req.body.passwordConfirm) {
    return next( new appError('This route is not for updating passwords, please use updateMyPassword.', 400))
  }

  // TODO need some kind of email verification going forward
  const filteredBody = filterObj(req.body, 'name', 'email')
  if (req.file) {
    filteredBody.photo = req.file.filename

    // How to set to default on error? - return old doc with update
    // Model.find() returns an array of objects
    let photoName = await User.find({_id: req.user.id}, 'photo')
    // console.log('User photo filename: ', photoName[0].photo)
    if (photoName[0].photo !== 'default_avatar.jpeg'){
      await deletePhoto(`public/img/users/${photoName[0].photo}`)
    }
  }

  // req.user set in authController#protect
  const updatedUser = await User.findByIdAndUpdate(req.user.id, filteredBody, {
    new: true,
    runValidators: true
  })

  res.status(200).json({
    status: 'success',
    data: {
      updatedUser
    }
  })
})

exports.deleteMe = catchAsync(async (req, res, next) => {
  await User.findByIdAndUpdate(req.user.id, { active: false })

  res.status(204).json({
    status: 'success',
    data: null
  })
})

// Set to admin only
exports.getAllUsers = routeFactory.getAll(User)
exports.getUser = routeFactory.getOne(User)
exports.createUser = routeFactory.createOne(User)
exports.updateUser = routeFactory.updateOne(User)
exports.deleteUser = routeFactory.deleteOne(User)
