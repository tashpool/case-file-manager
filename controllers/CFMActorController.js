const CFMActor = require('../models/CFMActor')
const { body, validationResult } = require('express-validator')
handlerFactory = require('./../controllers/handlerFactory')

exports.cfmactor_list = handlerFactory.getAll(CFMActor)
exports.cfmactor_detail = handlerFactory.getOne(CFMActor)

exports.cfmactor_create = [
  body('foreName').isLength( { min: 1, max: 100 }).trim().withMessage('A fore name is required.'),
  body('surName').isLength( { min: 1, max: 100 }).trim().withMessage('A sur name is required.'),

  async function(req, res, next) {
    const validationErrorChecks = validationResult(req)

    if(!validationErrorChecks.isEmpty()) {
      console.log(`Actor Create Validation Check Failed`)
      const err = new Error(JSON.stringify(validationErrorChecks))
      err.status = 'error'
      err.statusCode = 422

      return next(err)
    }

    let newActor = new CFMActor(
      {
        foreName: req.body.foreName,
        surName: req.body.surName,
        gender: req.body.gender,
        description: req.body.description,
        title: req.body.title,
        birthDate: req.body.birthDate,
        deathDate: req.body.deathDate,
        photo: req.body.photo || 'default_actor.jpeg'
      }
    )

    try {
      let savedActor = await newActor.save()
      res.status(201).json({
        status: "success",
        msg: 'Actor added to system.',
        data: {
          id: savedActor._id
        }
      })
    } catch(tryError) {
      tryError.status = 'error'
      tryError.statusCode = 500
      return next(tryError)
    }
  }
]

exports.cfmactor_update = handlerFactory.updateOne(CFMActor)
exports.cfmactor_delete = handlerFactory.deleteOne(CFMActor)