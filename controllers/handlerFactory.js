const catchAsync = require('./../utils/catchAsync')
const appError = require('./../utils/appError')
const apiFeatures = require('./../utils/apiFiltering')
const { deletePhoto } = require('./../utils/photo')

exports.createOne = Model =>
  catchAsync(async (req, res, next) => {
    // console.log('💥 -- Create Request -- 💥')
    // console.log(req.body)
    // console.log('💥 -- Create Request -- 💥')

    const doc = await Model.create(req.body)

    if (!doc) return next(new appError('Error creating doc.', 500))

    res.status(201).json({
      status: 'success',
      data: {
        data: doc
      }
    })
  })

exports.deleteOne = Model =>
  catchAsync(async(req, res, next) => {
    const doc = await Model.findByIdAndDelete(req.params.id)

    if (!doc) return next(new appError('No document found with that ID.', 404))

    res.status(204).json({
      status: 'success',
      data: null
    })
  })

exports.updateOne = Model =>
  catchAsync(async(req, res, next) => {
    // Update photo name in req object and 
    // propagate to Model Document
    if (req.file) {
      req.body.photo = req.file.filename

      // How to set to default on error? - return old doc with update
      // Model.find() returns an array or objects
      let photoName = await Model.find({_id: req.params.id}, 'photo')
      // TODO Only actors uses this path for photos - need to update path
      if(photoName[0].photo !== 'default_actor.jpeg') {
        await deletePhoto(`public/img/actors/${photoName[0].photo}`)
      }
    }

    // Branch on Pushing an updated value into an array,
    // doPush? pushField.push( pushValue )
    // Evaluate variable into object key with square brackets
    let doc = undefined
    if (req.body.doPush) {
      let pushField = req.body.pushField
      let pushValue = req.body.pushValue

      doc = await Model.findByIdAndUpdate(
        req.params.id,
        { $push: { [pushField]: pushValue } },
        { new: true }
        )
    } else {
      doc = await Model.findByIdAndUpdate(req.params.id, req.body,
        {
          new: true,
          runValidators: true
        })
    }

    if (!doc) return next(new appError('No document found with that ID.', 404))

    res.status(200).json({
      status: 'success',
      data: {
        data: doc
      }
    })
  })

exports.getOne = (Model, popOptions) =>
  catchAsync(async(req, res, next) => {
    let query = await Model.findById(req.params.id)
    if (popOptions) query = query.populate(popOptions).execPopulate()
    const doc = await query

    if (!doc) return next(new appError('No document found with that ID.', 404))

    res.status(200).json({
      status: 'success',
      data: {
        data: doc
      }
    })
  })

exports.getAll = Model =>
  catchAsync(async(req, res, next) => {
    // TODO write up a better explanation here, nested GETs
    let filter = {}
    if (req.params.caseId) filter = { case: req.params.caseId }

    const features = new apiFeatures(Model.find(filter), req.query)
      .filter()
      .sort()
      .limitFields()
      .paginate()

    const doc = await features.query

    res.status(200).json({
      status: 'success',
      results: doc.length,
      data: {
        data: doc
      }
    })
  })

// Populate a subset of documents within an array of ids
exports.getSome = Model =>
  catchAsync(async(req, res, next) => {
    const documentIDs = req.body.documentIDs

    const records = await Model.find().where('_id').in(documentIDs).exec()

    res.status(200).json({
      status: 'success',
      results: records.length,
      data: {
        data: records
      }
    })
  })