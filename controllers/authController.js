const User = require('./../models/CFMUser')
const jwt = require('jsonwebtoken')
const { promisify } = require('util')
const catchAsync = require('./../utils/catchAsync')
const appError = require('./../utils/appError')
const sendEmail = require('./../utils/email')
const crypto = require('crypto')

const signToken = id => {
  return jwt.sign({ id },
    process.env.JWT_SECRET,
    { expiresIn: process.env.JWT_EXPIRES_IN})
}

/*
https://expressjs.com/en/api.html#res.cookie
 */
const createSendToken = (user, statusCode, res) => {
  const token = signToken(user._id)
  const cookieOptions = {
    expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000),
    httpOnly: true
  }
  if (process.env.NODE_ENV === 'production') cookieOptions.secure = true

  res.cookie('jwt', token, cookieOptions)

  // Keep the password out of the response data, the JWT will identify the user
  user.password = undefined

  res.status(statusCode).json({
    status: 'success',
    token,
    data: {
      user
    }
  })
}

/**
 * Upon token and user verification, req.user is set with user db entry
 */
exports.protect = catchAsync( async(req, res, next) => {
  // 1. Check for a token
  let token
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
    token = req.headers.authorization.split(' ')[1]
  }

  if (!token) { return next(new appError('Please log in to get access'), 401) }

  // 2. Decode the token given
  const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET)

  // 3. Does the user exist?
  // Has user changed their password, or their account was deleted
  const currentUser = await User.findById(decoded.id)
  if (!currentUser) {
    return next(new appError('The user for this token no longer exists.'), 401)
  }

  // 4. Did the user password change AFTER the token was issued?
  if (currentUser.changedPasswordAfter(decoded.iat)) {
    return next(new appError('User recently changed password, please log in again.'), 401)
  }

  // Set decoded user info to req object and grant access to protected route
  req.user = currentUser
  next()
})

/**
 * @param roles - Strings of whitelisted user roles for access
 */
exports.restrictTo = (...roles) => {
  // Can't pass params directly, so wrapping the function
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return next(new appError('You do not have permission to complete this action.', 403))
    }

    next()
  }
}

exports.signup = catchAsync( async(req, res, next) => {
  const newUser = await User.create({
    name: req.body.name,
    email: req.body.email,
    photo: req.body.photo,
    password: req.body.password,
    passwordConfirm: req.body.passwordConfirm
  })

  createSendToken(newUser, 201, res)
})

exports.login = catchAsync( async(req, res, next) => {
  const { email, password } = req.body

  // 1. Email & Pass are required
  if (!email || !password) {
    return next(new appError('Email and password are required.', 400))
  }

  // 2. Find user by email
  const user = await User.findOne({ email }).select('+password')

  // 3a. User exists
  // 3b. Password is validated
  if(!user || !(await user.correctPassword(password, user.password))) {
    return next(new appError('Incorrect email or password', 401))
  }

  createSendToken(user, 200, res)
})

exports.forgotPassword = catchAsync( async(req, res, next) => {
  // 1. Get user from posted email
  const user = await User.findOne({ email: req.body.email })

  if (!user) {
    return next(new appError('There is no user with that email address.', 404))
  }
  // 2. Generate random token and save to user doc
  const resetToken = user.createPasswordResetToken()
  await user.save({ validateBeforeSave: false })

  // 3. Send back reset token in email
  const resetURL = `${req.protocol}://${req.get('host')}/users/resetPassword/${resetToken}`
  const message = `Forgot your password? Submit a PATCH request with your new password and password confirmation to: ${resetURL}.\n\nIf you didn't forget your password, you may ignore this email.`

  try {
    await sendEmail({
      email: user.email,
      subject: 'Your password reset token, valid for 10 minutes.',
      message
    })

    res.status(200).json({
      status: 'success',
      message: 'Token sent to email.'
    })
  } catch(err) {
    user.passwordResetToken = undefined
    user.passwordResetExpires = undefined
    await user.save({ validateBeforeSave: false })

    return next(new appError('There was an error sending email, please try again later.', 500))
  }
})

exports.resetPassword = catchAsync(async(req, res, next) => {
  // 1. Get the user by given token
  // Same code from User Model, refactor?
  const hashedToken = crypto.createHash('sha256').update(req.params.token).digest('hex')

  const user = await User.findOne({ passwordResetToken: hashedToken,
    passwordResetExpires: { $gt: Date.now() } })

  if (!user) {
    return next(new appError('Token is invalid or has expired.', 400))
  }

  // 2. Set new password fields for valid user
  user.password = req.body.password
  user.passwordConfirm = req.body.passwordConfirm
  user.passwordResetToken = undefined
  user.passwordResetExpires = undefined
  // make sure to keep validations, we want them checked this time
  await user.save()

  // 3. Update changed password field for user

  // 4. Log the user in (Send JWT to client)
  createSendToken(user, 200, res)
})
/*
Receive 'passwordCurrent' in body from POST
 */
exports.updatePassword = catchAsync(async(req, res, next) => {
  // 1. Who is this? Find the current user
  const user = await User.findById(req.user.id).select('+password')

  // console.log(`PCurrent: ${req.body.passwordCurrent}`)
  // console.log(`UPass: ${user.password}`)

  // 2. Correct Password?
  if (!(await user.correctPassword(req.body.passwordCurrent, user.password))) {
    return next(new appError('The current password is incorrect.'), 401)
  }

  // 3. Update password
  user.password = req.body.password
  user.passwordConfirm = req.body.passwordConfirm
  // Must use save to trigger model validations and pre saves
  await user.save()

  // 4. Log in the user
  createSendToken(user, 200, res)
})