const CFMCase = require('../models/CFMCases')
const CFMEvent = require('../models/CFMEvent')
const { check, body, validationResult } = require('express-validator')
const catchAsync = require('./../utils/catchAsync')
const appError = require('./../utils/appError')
const handlerFactory = require('./handlerFactory')

exports.cfmcase_list = handlerFactory.getAll(CFMCase)

// Don't know how to populate three levels, caseID -> All Events -> All Actors
// Need some better mongo-gung-fu, so custom function for all actors under a case
exports.cfmcase_get_actors = catchAsync( async function (req, res, next) {
  const doesCaseExist = await CFMCase.exists({ _id: req.params.id })

  if (!doesCaseExist) {
    return next(new appError('Case does not exist.', 422))
  } else {
    let allActors = new Set()
    const cfmcase = await CFMCase.findById(req.params.id).exec()
    // cfmcase.event is an array of object_ids
    if (cfmcase.event && cfmcase.event.length) {
      for (const event of cfmcase.event) {
        let currentActors = await CFMEvent.findById(event, 'actor').exec()
        // console.log(`CurrentActors: ${currentActors.actor}, ${typeof currentActors.actor}`)
        // currentActors is an object { actor: [], _id: ... }
        for (const eachActor of currentActors.actor) {
          // Working with Mongoose Array type, type cast to string
          allActors.add(eachActor._id.toString())
        }
      }
    }

    res.status(200).json({
      status: 'success',
      msg: 'All Actor IDs for this case',
      data: {
        actors: [...allActors]
      }
    })
  }
})

// handlerFactory not used since generating Events when fetching
exports.cfmcase_detail = catchAsync( async function(req, res, next) {
    const doesCaseExist = await CFMCase.exists({ _id: req.params.id })

    if (!doesCaseExist) {
      return next(new appError('Case does not exist.', 422))
    } else {
      const allCaseEvents = []
      const cfmcase = await CFMCase.findById(req.params.id)
      if (cfmcase.event && cfmcase.event.length) {
        for (const event of cfmcase.event) {
          allCaseEvents.push(await CFMEvent.findById(event, 'title activity description'))
        }
      }

      res.status(200).json({
        status: 'success',
        data: {
          case: cfmcase,
          events: allCaseEvents
        }
      })
    }
})

// A post call to authController.protect is **Required**,
// to set the owner of the case, req.user._id
exports.cfmcase_create = [
  body('title').isLength({ min: 1, max: 100 }).trim().escape().withMessage('Title is required.'),
    // .matches(/^[a-z0-9 ]+$/, 'gi').withMessage('Title has invalid characters.'),
  body('description').isLength({ min: 1, max: 500 }).trim().escape().withMessage('Description is required.'),
  //   .matches(/!([^0-9A-z _.,!']+)/, 'gi').withMessage('Description has invalid characters.'),
  check('openDate').optional({nullable: true}).toDate(),

  async function(req, res, next) {
    // console.log(`Req Object: ${JSON.stringify(req.body)}`)

    const validationErrorChecks = validationResult(req)

    if(!validationErrorChecks.isEmpty()) {
      console.log(`Case Create Validation Check Failed`)
      const err = new Error(JSON.stringify(validationErrorChecks))
      err.status = 'error'
      err.statusCode = 422

      return next(err)
    }

    let newCase = new CFMCase(
      {
        event: [],
        owner: req.user._id,
        title: req.body.title,
        description: req.body.description,
        openDate: req.body.openDate
      }
    )

    try {
      let savedCase = await newCase.save()
       res.status(201).json({
        status: "success",
         msg: 'Case added to system.',
         data: {
          id: savedCase._id
         }
      })
    } catch(tryError) {
      tryError.status = 'error'
      tryError.statusCode = 500
      return next(tryError)
    }
  }
]

exports.cfmcase_update = handlerFactory.updateOne(CFMCase)

// TODO Decide if I should delete linked events as well?
// async functions return a promise (resolve, reject)
exports.cfmcase_delete = handlerFactory.deleteOne(CFMCase)