const CFMEvent = require('../models/CFMEvent')
const { body, validationResult } = require('express-validator')
const handlerFactory = require('../controllers/handlerFactory')

exports.cfmevent_list = handlerFactory.getAll(CFMEvent)
exports.cfmevent_detail = handlerFactory.getOne(CFMEvent, 'actor evidence')

/*
Two references, actor and evidence.
Assume we have their _ids and insert references.
Front end will need to call creation methods for new ones beforehand.
 */
exports.cfmevent_create = [
  body('title').isLength( {min: 1, max: 100})
    .trim().withMessage('Title is required.'),
  body('description').isLength({min: 1, max: 500})
    .trim().withMessage('Description is required.'),
  body('startTime').exists().toDate(),

  async function(req, res, next) {
    // console.log(`Req Object: ${JSON.stringify(req.body)}`)

    const validationErrorChecks = validationResult(req)

    if(!validationErrorChecks.isEmpty()) {
      console.log(`Event Create Validation Check Failed`)
      const err = new Error(JSON.stringify(validationErrorChecks))
      err.status = 'error'
      err.statusCode = 422

      return next(err)
    }

    // Actors is sent, but not an Array
    if (req.body.actor && !Array.isArray(req.body.actor)) {
      const err = new Error('Actors must be an array of Object IDs.')
      err.status = 'error'
      err.statusCode = 422

      return next(err)
    }
    // Evidence is sent, but not an Array
    if (req.body.evidence && !Array.isArray(req.body.evidence)) {
      const err = new Error('Evidence must be an array of Object IDs.')
      err.status = 'error'
      err.statusCode = 422

      return next(err)
    }

    // Finished Checks, create and save new Document
    let newEvent = new CFMEvent(
      {
        title: req.body.title,
        activity: req.body.activity,
        description: req.body.description,
        actor: req.body.actor,
        startTime: req.body.startTime,
        endTime: req.body.endTime,
        location: {
          coordinates: req.body.location.coordinates,
          description: req.body.location.locDesc
        },
        evidence: req.body.evidence,
        linkedCase: req.body.linkedCase
      }
    )

    try {
      let savedEvent = await newEvent.save()
      res.status(200).json({
        status: "success",
        msg: "Event added to system.",
        data: {
          savedEvent
        }
      })
    } catch(tryError) {
      tryError.status = 'error'
      tryError.statusCode = 500
      return next(tryError)
    }
  }
]

exports.cfmevent_update = handlerFactory.updateOne(CFMEvent)
exports.cfmevent_delete = handlerFactory.deleteOne(CFMEvent)
exports.cfmevent_getsome = handlerFactory.getSome(CFMEvent)