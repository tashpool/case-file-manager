import Vue from 'vue'
import VueRouter from 'vue-router'
import ShowCases from '@/views/ShowCases'
import LoginUser from '@/views/LoginUser'
import RegisterUser from '@/views/RegisterUser'
import CaseDetail from '@/views/CaseDetail'
import EventDetail from '@/views/EventDetail'
import ActorDetail from '@/views/ActorDetail'
import EvidenceDetail from '@/views/EvidenceDetail'
import AboutMe from '@/views/AboutMe'
import CreateCase from '@/views/CreateCase'
import CreateEvent from '@/views/CreateEvent'
import CreateEvidence from '@/views/CreateEvidence'
import CreateActor from '@/views/CreateActor'
import CaseTimeline from '@/views/CaseTimeline'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'ShowCases',
    component: ShowCases
  },
  {
    path: '/case-detail/:caseID',
    name: 'CaseDetail',
    component: CaseDetail,
    props: true,
    // meta: { requiresAuth: true }
  },
  {
    path: '/event-detail/:eventID',
    name: 'EventDetail',
    component: EventDetail,
    props: true,
    // meta: { requiresAuth: true }
  },
  {
    path: '/actor-detail/:actorID',
    name: 'ActorDetail',
    component: ActorDetail,
    props: true,
    // meta: { requiresAuth: true }
  },
  {
    path: '/evidence-detail/:eviID',
    name: 'EvidenceDetail',
    component: EvidenceDetail,
    props: true,
    // meta: { requiresAuth: true }
  },
  {
    path: '/create-case',
    name: 'CreateCase',
    component: CreateCase,
    meta: { requiresAuth: true }
  },
  {
    path: '/create-event',
    name: 'CreateEvent',
    component: CreateEvent,
    meta: { requiresAuth: true }
  },
  {
    path: '/create-evidence',
    name: 'CreateEvidence',
    component: CreateEvidence,
    meta: { requiresAuth: true }
  },
  {
    path: '/create-actor',
    name: 'CreateActor',
    component: CreateActor,
    meta: { requiresAuth: true }
  },
  {
    path: '/about-me',
    name: 'AboutMe',
    component: AboutMe,
    meta: { requiresAuth: true }
  },
  {
    path: '/login',
    name: 'LoginUser',
    component: LoginUser
  },
  {
    path: '/register',
    name: 'RegisterUser',
    component: RegisterUser
  },
  {
    path: '/case-timeline/:caseID',
    name: 'CaseTimeline',
    component: CaseTimeline,
    props: true
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  // console.log(`localStorage.getItem(user) : ${localStorage.getItem('user')}`)
  const loggedIn = localStorage.getItem('user')

  if (to.matched.some(record => record.meta.requiresAuth) && !loggedIn) {
    next({ name: 'LoginUser'} )
  }
  next()
})

export default router