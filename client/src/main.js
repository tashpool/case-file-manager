import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router/index'
import store from '@/vuex/store'

Vue.config.productionTip = false

// Global Variable(s)
// TODO Change on deployment
// Vue.prototype.$publicServer = 'http://127.0.0.1:8081/'
// Vue.prototype.$publicServer = '../public/'
Vue.prototype.$debugInfo = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')