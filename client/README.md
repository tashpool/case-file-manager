# Case File Manager Client

## Overview
A Case File Management system, CFM, is used to enter and create reports on incidents. 

A detective walks onto a crime scene and begins taking notes. Messy and disorganized notes that they have to put together back at the office taking hours, or even days.
Trying to put together that thriller novel and figure out whodunit? Map it out with individual events, that contain actors, evidence, and links to them!

This project is a mostly static Vue front end to a fully functional backend API written in Node. It was created to help teach myself more about web app creation and deployment. Definitely a work in progress.  

## Project setup Vue Front End
```
npm install
```
Then set the publicPath and API location variables to a working CFM Backend install.

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Author and Repo
[https://gitlab.com/tashpool/cfm-mevn/](https://gitlab.com/tashpool/cfm-mevn/)